==================================
 RsNgx
==================================

.. image:: https://img.shields.io/pypi/v/RsNgx.svg
   :target: https://pypi.org/project/ RsNgx/

.. image:: https://readthedocs.org/projects/sphinx/badge/?version=master
   :target: https://RsNgx.readthedocs.io/

.. image:: https://img.shields.io/pypi/l/RsNgx.svg
   :target: https://pypi.python.org/pypi/RsNgx/

.. image:: https://img.shields.io/pypi/pyversions/pybadges.svg
   :target: https://img.shields.io/pypi/pyversions/pybadges.svg

.. image:: https://img.shields.io/pypi/dm/RsNgx.svg
   :target: https://pypi.python.org/pypi/RsNgx/

Rohde & Schwarz NGx Power Supply RsNgx instrument driver.

Basic Hello-World code:

.. code-block:: python

    from RsNgx import *

    instr = RsNgx('TCPIP::192.168.2.101::hislip0')
    idn = instr.query('*IDN?')
    print('Hello, I am: ' + idn)

Check out the full documentation on `ReadTheDocs <https://RsNgx.readthedocs.io/>`_.

Supported instruments: NGU, NGL, NGM, NGP

The package is hosted here: https://pypi.org/project/RsNgx/

Documentation: https://RsNgx.readthedocs.io/

Examples: https://github.com/Rohde-Schwarz/Examples/tree/main/Misc/Python/RsNgx_ScpiPackage


Version history
----------------

	Latest release notes summary: Docu Fixes

	Version 3.1.0
		- Docu Fixes

	Version 3.1.0.49
		- Rebuilt with new driver core

	Version 3.1.0.48
		- Update for NGP800 FW 2.015

	Version 3.0.0.39
		- First released version for NGU FW 3.0
