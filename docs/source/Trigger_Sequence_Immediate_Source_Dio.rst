Dio
----------------------------------------





.. autoclass:: RsNgx.Implementations.Trigger.Sequence.Immediate.Source.Dio.DioCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Sequence_Immediate_Source_Dio_Channel.rst
	Trigger_Sequence_Immediate_Source_Dio_Pin.rst