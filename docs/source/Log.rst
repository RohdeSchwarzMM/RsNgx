Log
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LOG:LOCation
	single: LOG:DATA
	single: LOG:TRIGgered
	single: LOG:STATe

.. code-block:: python

	LOG:LOCation
	LOG:DATA
	LOG:TRIGgered
	LOG:STATe



.. autoclass:: RsNgx.Implementations.Log.LogCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Log_Channel.rst
	Log_Count.rst
	Log_Duration.rst
	Log_Fname.rst
	Log_Interval.rst
	Log_Mode.rst
	Log_Stime.rst