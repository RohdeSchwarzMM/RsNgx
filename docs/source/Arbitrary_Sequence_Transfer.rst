Transfer
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ARBitrary:SEQuence:TRANsfer

.. code-block:: python

	ARBitrary:SEQuence:TRANsfer



.. autoclass:: RsNgx.Implementations.Arbitrary.Sequence.Transfer.TransferCls
	:members:
	:undoc-members:
	:noindex: