Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALibration:CURRent:DATA

.. code-block:: python

	CALibration:CURRent:DATA



.. autoclass:: RsNgx.Implementations.Calibration.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calibration_Current_Imax.rst
	Calibration_Current_Imin.rst