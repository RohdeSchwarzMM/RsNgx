Model
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: BATTery:MODel:SAVE
	single: BATTery:MODel:LOAD
	single: BATTery:MODel:TRANsfer
	single: BATTery:MODel:CAPacity
	single: BATTery:MODel:ISOC
	single: BATTery:MODel:CLEar

.. code-block:: python

	BATTery:MODel:SAVE
	BATTery:MODel:LOAD
	BATTery:MODel:TRANsfer
	BATTery:MODel:CAPacity
	BATTery:MODel:ISOC
	BATTery:MODel:CLEar



.. autoclass:: RsNgx.Implementations.Battery.Model.ModelCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Battery_Model_Current.rst
	Battery_Model_Data.rst
	Battery_Model_Fname.rst