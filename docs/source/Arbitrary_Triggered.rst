Triggered
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ARBitrary:TRIGgered:STATe
	single: ARBitrary:TRIGgered:MODE

.. code-block:: python

	ARBitrary:TRIGgered:STATe
	ARBitrary:TRIGgered:MODE



.. autoclass:: RsNgx.Implementations.Arbitrary.Triggered.TriggeredCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Arbitrary_Triggered_Group.rst
	Arbitrary_Triggered_Point.rst