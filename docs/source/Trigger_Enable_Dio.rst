Dio<DigitalIo>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.trigger.enable.dio.repcap_digitalIo_get()
	driver.trigger.enable.dio.repcap_digitalIo_set(repcap.DigitalIo.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:ENABle:DIO<DigitalIo>

.. code-block:: python

	TRIGger:ENABle:DIO<DigitalIo>



.. autoclass:: RsNgx.Implementations.Trigger.Enable.Dio.DioCls
	:members:
	:undoc-members:
	:noindex: