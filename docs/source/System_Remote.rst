Remote
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:REMote

.. code-block:: python

	SYSTem:REMote



.. autoclass:: RsNgx.Implementations.System.Remote.RemoteCls
	:members:
	:undoc-members:
	:noindex: