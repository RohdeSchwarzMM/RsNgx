Fname
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ARBitrary:BLOCk:FNAMe

.. code-block:: python

	ARBitrary:BLOCk:FNAMe



.. autoclass:: RsNgx.Implementations.Arbitrary.Block.Fname.FnameCls
	:members:
	:undoc-members:
	:noindex: