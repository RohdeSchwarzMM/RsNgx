Upper
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:CURRent:LEVel:IMMediate:ALIMit:UPPer

.. code-block:: python

	SOURce:CURRent:LEVel:IMMediate:ALIMit:UPPer



.. autoclass:: RsNgx.Implementations.Source.Current.Level.Immediate.Alimit.Upper.UpperCls
	:members:
	:undoc-members:
	:noindex: