Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:RESistance:LEVel:IMMediate:AMPLitude

.. code-block:: python

	SOURce:RESistance:LEVel:IMMediate:AMPLitude



.. autoclass:: RsNgx.Implementations.Source.Resistance.Level.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex: