Discard
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:LAN:DISCard

.. code-block:: python

	SYSTem:COMMunicate:LAN:DISCard



.. autoclass:: RsNgx.Implementations.System.Communicate.Lan.Discard.DiscardCls
	:members:
	:undoc-members:
	:noindex: