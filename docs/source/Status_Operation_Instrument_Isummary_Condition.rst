Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:OPERation:INSTrument:ISUMmary<Channel>:CONDition

.. code-block:: python

	STATus:OPERation:INSTrument:ISUMmary<Channel>:CONDition



.. autoclass:: RsNgx.Implementations.Status.Operation.Instrument.Isummary.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex: