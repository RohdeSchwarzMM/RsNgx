Behavior
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ARBitrary:SEQuence:BEHavior:END

.. code-block:: python

	ARBitrary:SEQuence:BEHavior:END



.. autoclass:: RsNgx.Implementations.Arbitrary.Sequence.Behavior.BehaviorCls
	:members:
	:undoc-members:
	:noindex: