Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:ENABle:GENeral

.. code-block:: python

	TRIGger:ENABle:GENeral



.. autoclass:: RsNgx.Implementations.Trigger.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Enable_Dio.rst
	Trigger_Enable_Select.rst