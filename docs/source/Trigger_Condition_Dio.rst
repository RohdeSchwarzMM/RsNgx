Dio<DigitalIo>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.trigger.condition.dio.repcap_digitalIo_get()
	driver.trigger.condition.dio.repcap_digitalIo_set(repcap.DigitalIo.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:CONDition:DIO<DigitalIo>

.. code-block:: python

	TRIGger:CONDition:DIO<DigitalIo>



.. autoclass:: RsNgx.Implementations.Trigger.Condition.Dio.DioCls
	:members:
	:undoc-members:
	:noindex: