Condition
----------------------------------------





.. autoclass:: RsNgx.Implementations.Trigger.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Condition_Dio.rst