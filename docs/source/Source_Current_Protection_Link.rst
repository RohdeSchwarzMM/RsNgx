Link
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:CURRent:PROTection:LINK

.. code-block:: python

	SOURce:CURRent:PROTection:LINK



.. autoclass:: RsNgx.Implementations.Source.Current.Protection.Link.LinkCls
	:members:
	:undoc-members:
	:noindex: