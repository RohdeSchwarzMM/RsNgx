Instrument
----------------------------------------





.. autoclass:: RsNgx.Implementations.Instrument.InstrumentCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Instrument_Select.rst