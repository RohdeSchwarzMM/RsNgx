Ainput
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:VOLTage:AINPut:STATe
	single: SOURce:VOLTage:AINPut:INPut

.. code-block:: python

	SOURce:VOLTage:AINPut:STATe
	SOURce:VOLTage:AINPut:INPut



.. autoclass:: RsNgx.Implementations.Source.Voltage.Ainput.AinputCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Voltage_Ainput_Triggered.rst