Apply
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: APPLy

.. code-block:: python

	APPLy



.. autoclass:: RsNgx.Implementations.Apply.ApplyCls
	:members:
	:undoc-members:
	:noindex: