Signal
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIO:OUTPut:SIGNal

.. code-block:: python

	DIO:OUTPut:SIGNal



.. autoclass:: RsNgx.Implementations.Dio.Output.Signal.SignalCls
	:members:
	:undoc-members:
	:noindex: