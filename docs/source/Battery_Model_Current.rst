Current
----------------------------------------





.. autoclass:: RsNgx.Implementations.Battery.Model.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Battery_Model_Current_Limit.rst