Instrument
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:OPERation:INSTrument:EVENt
	single: STATus:OPERation:INSTrument:CONDition
	single: STATus:OPERation:INSTrument:PTRansition
	single: STATus:OPERation:INSTrument:NTRansition
	single: STATus:OPERation:INSTrument:ENABle

.. code-block:: python

	STATus:OPERation:INSTrument:EVENt
	STATus:OPERation:INSTrument:CONDition
	STATus:OPERation:INSTrument:PTRansition
	STATus:OPERation:INSTrument:NTRansition
	STATus:OPERation:INSTrument:ENABle



.. autoclass:: RsNgx.Implementations.Status.Operation.Instrument.InstrumentCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Status_Operation_Instrument_Isummary.rst