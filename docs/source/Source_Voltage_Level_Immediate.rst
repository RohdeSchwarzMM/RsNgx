Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:VOLTage:LEVel:IMMediate:AMPLitude

.. code-block:: python

	SOURce:VOLTage:LEVel:IMMediate:AMPLitude



.. autoclass:: RsNgx.Implementations.Source.Voltage.Level.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Voltage_Level_Immediate_Alimit.rst
	Source_Voltage_Level_Immediate_Step.rst