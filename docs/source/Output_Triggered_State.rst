State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut:TRIGgered:STATe

.. code-block:: python

	OUTPut:TRIGgered:STATe



.. autoclass:: RsNgx.Implementations.Output.Triggered.State.StateCls
	:members:
	:undoc-members:
	:noindex: