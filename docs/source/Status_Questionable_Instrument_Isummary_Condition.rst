Condition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:INSTrument:ISUMmary<Channel>:CONDition

.. code-block:: python

	STATus:QUEStionable:INSTrument:ISUMmary<Channel>:CONDition



.. autoclass:: RsNgx.Implementations.Status.Questionable.Instrument.Isummary.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex: