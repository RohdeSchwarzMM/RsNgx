State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:STATe

.. code-block:: python

	TRIGger:STATe



.. autoclass:: RsNgx.Implementations.Trigger.State.StateCls
	:members:
	:undoc-members:
	:noindex: