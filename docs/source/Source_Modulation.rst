Modulation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:MODulation

.. code-block:: python

	SOURce:MODulation



.. autoclass:: RsNgx.Implementations.Source.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Modulation_Gain.rst