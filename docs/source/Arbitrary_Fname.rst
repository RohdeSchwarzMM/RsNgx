Fname
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ARBitrary:FNAMe

.. code-block:: python

	ARBitrary:FNAMe



.. autoclass:: RsNgx.Implementations.Arbitrary.Fname.FnameCls
	:members:
	:undoc-members:
	:noindex: