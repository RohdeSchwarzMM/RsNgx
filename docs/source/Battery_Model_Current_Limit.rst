Limit
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: BATTery:MODel:CURRent:LIMit:EOD
	single: BATTery:MODel:CURRent:LIMit:REGular
	single: BATTery:MODel:CURRent:LIMit:EOC

.. code-block:: python

	BATTery:MODel:CURRent:LIMit:EOD
	BATTery:MODel:CURRent:LIMit:REGular
	BATTery:MODel:CURRent:LIMit:EOC



.. autoclass:: RsNgx.Implementations.Battery.Model.Current.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex: