Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:VOLTage:NEGative:LEVel:IMMediate:AMPLitude

.. code-block:: python

	SOURce:VOLTage:NEGative:LEVel:IMMediate:AMPLitude



.. autoclass:: RsNgx.Implementations.Source.Voltage.Negative.Level.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex: