Touch
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:TOUCh:STATe

.. code-block:: python

	SYSTem:TOUCh:STATe



.. autoclass:: RsNgx.Implementations.System.Touch.TouchCls
	:members:
	:undoc-members:
	:noindex: