Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:INSTrument:ISUMmary<Channel>:ENABle

.. code-block:: python

	STATus:QUEStionable:INSTrument:ISUMmary<Channel>:ENABle



.. autoclass:: RsNgx.Implementations.Status.Questionable.Instrument.Isummary.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: