Operation
----------------------------------------





.. autoclass:: RsNgx.Implementations.Status.Operation.OperationCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Status_Operation_Instrument.rst