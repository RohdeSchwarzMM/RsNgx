Self
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALibration:SELF

.. code-block:: python

	CALibration:SELF



.. autoclass:: RsNgx.Implementations.Calibration.Self.SelfCls
	:members:
	:undoc-members:
	:noindex: