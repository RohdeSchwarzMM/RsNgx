Protection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:VOLTage:PROTection:STATe
	single: SOURce:VOLTage:PROTection:LEVel
	single: SOURce:VOLTage:PROTection:TRIPped
	single: SOURce:VOLTage:PROTection:CLEar

.. code-block:: python

	SOURce:VOLTage:PROTection:STATe
	SOURce:VOLTage:PROTection:LEVel
	SOURce:VOLTage:PROTection:TRIPped
	SOURce:VOLTage:PROTection:CLEar



.. autoclass:: RsNgx.Implementations.Source.Voltage.Protection.ProtectionCls
	:members:
	:undoc-members:
	:noindex: