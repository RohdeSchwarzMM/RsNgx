Channel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LOG:CHANnel:STATe

.. code-block:: python

	LOG:CHANnel:STATe



.. autoclass:: RsNgx.Implementations.Log.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex: