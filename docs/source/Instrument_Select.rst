Select
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INSTrument:NSELect

.. code-block:: python

	INSTrument:NSELect



.. autoclass:: RsNgx.Implementations.Instrument.Select.SelectCls
	:members:
	:undoc-members:
	:noindex: