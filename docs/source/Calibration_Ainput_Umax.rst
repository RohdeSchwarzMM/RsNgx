Umax
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALibration:AINPut:UMAX

.. code-block:: python

	CALibration:AINPut:UMAX



.. autoclass:: RsNgx.Implementations.Calibration.Ainput.Umax.UmaxCls
	:members:
	:undoc-members:
	:noindex: