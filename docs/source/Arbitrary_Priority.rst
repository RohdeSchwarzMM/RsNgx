Priority
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ARBitrary:PRIority:MODE

.. code-block:: python

	ARBitrary:PRIority:MODE



.. autoclass:: RsNgx.Implementations.Arbitrary.Priority.PriorityCls
	:members:
	:undoc-members:
	:noindex: