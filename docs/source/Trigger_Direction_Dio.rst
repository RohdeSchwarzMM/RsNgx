Dio<DigitalIo>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.trigger.direction.dio.repcap_digitalIo_get()
	driver.trigger.direction.dio.repcap_digitalIo_set(repcap.DigitalIo.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:DIRection:DIO<DigitalIo>

.. code-block:: python

	TRIGger:DIRection:DIO<DigitalIo>



.. autoclass:: RsNgx.Implementations.Trigger.Direction.Dio.DioCls
	:members:
	:undoc-members:
	:noindex: