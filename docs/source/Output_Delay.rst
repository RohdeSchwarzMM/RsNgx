Delay
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut:DELay:DURation
	single: OUTPut:DELay:STATe

.. code-block:: python

	OUTPut:DELay:DURation
	OUTPut:DELay:STATe



.. autoclass:: RsNgx.Implementations.Output.Delay.DelayCls
	:members:
	:undoc-members:
	:noindex: