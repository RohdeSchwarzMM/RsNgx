Delay
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FUSE:DELay:INITial

.. code-block:: python

	FUSE:DELay:INITial



.. autoclass:: RsNgx.Implementations.Fuse.Delay.DelayCls
	:members:
	:undoc-members:
	:noindex: