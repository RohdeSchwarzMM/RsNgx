Flog
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FLOG:STATe
	single: FLOG:DATA
	single: FLOG:TRIGgered
	single: FLOG:SRATe
	single: FLOG:STIMe
	single: FLOG:TARGet

.. code-block:: python

	FLOG:STATe
	FLOG:DATA
	FLOG:TRIGgered
	FLOG:SRATe
	FLOG:STIMe
	FLOG:TARGet



.. autoclass:: RsNgx.Implementations.Flog.FlogCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Flog_File.rst