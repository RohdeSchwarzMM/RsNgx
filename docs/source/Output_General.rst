General
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut:GENeral:STATe

.. code-block:: python

	OUTPut:GENeral:STATe



.. autoclass:: RsNgx.Implementations.Output.General.GeneralCls
	:members:
	:undoc-members:
	:noindex: