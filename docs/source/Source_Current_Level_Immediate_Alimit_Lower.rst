Lower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:CURRent:LEVel:IMMediate:ALIMit:LOWer

.. code-block:: python

	SOURce:CURRent:LEVel:IMMediate:ALIMit:LOWer



.. autoclass:: RsNgx.Implementations.Source.Current.Level.Immediate.Alimit.Lower.LowerCls
	:members:
	:undoc-members:
	:noindex: