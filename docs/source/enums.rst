Enums
=========

ArbEndBehavior
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbEndBehavior.HOLD
	# All values (2x):
	HOLD | OFF

ArbTrigMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbTrigMode.RUN
	# All values (2x):
	RUN | SINGle

CalibrationType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalibrationType.CURR
	# All values (4x):
	CURR | IMP | NCUR | VOLT

DefaultStep
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DefaultStep.DEF
	# All values (2x):
	DEF | DEFault

DioFaultSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DioFaultSource.CC
	# All values (6x):
	CC | CR | CV | OUTPut | PROTection | SINK

DioOutSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DioOutSource.FORCed
	# All values (3x):
	FORCed | OUTPut | TRIGger

DioSignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DioSignal.CONStant
	# All values (2x):
	CONStant | PULSe

FastLogSampleRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FastLogSampleRate.S001k
	# All values (6x):
	S001k | S010k | S050k | S100 | S250k | S500k

FastLogTarget
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FastLogTarget.SCPI
	# All values (2x):
	SCPI | USB

Filename
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Filename.DEF
	# All values (3x):
	DEF | EXT | INT

HcpyFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HcpyFormat.BMP
	# All values (2x):
	BMP | PNG

LogMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LogMode.COUNt
	# All values (4x):
	COUNt | DURation | SPAN | UNLimited

LowHigh
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LowHigh.HIGH
	# All values (2x):
	HIGH | LOW

MinOrMax
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MinOrMax.MAX
	# All values (4x):
	MAX | MAXimum | MIN | MINimum

PrioMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PrioMode.CPM
	# All values (2x):
	CPM | VPM

TriggerChannel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerChannel.ALL
	# All values (6x):
	ALL | CH1 | CH2 | CH3 | CH4 | NONE

TriggerCondition
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TriggerCondition.ANINput
	# Last value:
	value = enums.TriggerCondition.VMODe
	# All values (19x):
	ANINput | ARB | ARBGroup | ARBPoint | CMODe | ENABle | FUSE | ILEVel
	INHibit | LOG | OPP | OTP | OUTPut | OVP | PLEVel | RAMP
	STATistics | VLEVel | VMODe

TriggerDioSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerDioSource.EXT
	# All values (2x):
	EXT | IN

TriggerDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerDirection.INPut
	# All values (2x):
	INPut | OUTPut

TriggerOperMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerOperMode.CC
	# All values (5x):
	CC | CR | CV | PROTection | SINK

TriggerSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerSource.DIO
	# All values (3x):
	DIO | OMODe | OUTPut

UsbClass
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UsbClass.CDC
	# All values (2x):
	CDC | TMC

