Logic
----------------------------------------





.. autoclass:: RsNgx.Implementations.Trigger.Logic.LogicCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Logic_Dio.rst