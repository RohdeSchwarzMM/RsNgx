Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut:MODE:CAPacitance
	single: OUTPut:MODE

.. code-block:: python

	OUTPut:MODE:CAPacitance
	OUTPut:MODE



.. autoclass:: RsNgx.Implementations.Output.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: