Protection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:CURRent:PROTection:STATe
	single: SOURce:CURRent:PROTection:UNLink
	single: SOURce:CURRent:PROTection:TRIPped
	single: SOURce:CURRent:PROTection:CLEar

.. code-block:: python

	SOURce:CURRent:PROTection:STATe
	SOURce:CURRent:PROTection:UNLink
	SOURce:CURRent:PROTection:TRIPped
	SOURce:CURRent:PROTection:CLEar



.. autoclass:: RsNgx.Implementations.Source.Current.Protection.ProtectionCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Current_Protection_Delay.rst
	Source_Current_Protection_Link.rst