Pin
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:IMMediate:SOURce:DIO:PIN

.. code-block:: python

	TRIGger:SEQuence:IMMediate:SOURce:DIO:PIN



.. autoclass:: RsNgx.Implementations.Trigger.Sequence.Immediate.Source.Dio.Pin.PinCls
	:members:
	:undoc-members:
	:noindex: