Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:INSTrument:ISUMmary<Channel>:EVENt

.. code-block:: python

	STATus:QUEStionable:INSTrument:ISUMmary<Channel>:EVENt



.. autoclass:: RsNgx.Implementations.Status.Questionable.Instrument.Isummary.Event.EventCls
	:members:
	:undoc-members:
	:noindex: