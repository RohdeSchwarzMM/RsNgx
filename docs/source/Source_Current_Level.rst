Level
----------------------------------------





.. autoclass:: RsNgx.Implementations.Source.Current.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Current_Level_Immediate.rst