Source
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIO:OUTPut:SOURce

.. code-block:: python

	DIO:OUTPut:SOURce



.. autoclass:: RsNgx.Implementations.Dio.Output.Source.SourceCls
	:members:
	:undoc-members:
	:noindex: