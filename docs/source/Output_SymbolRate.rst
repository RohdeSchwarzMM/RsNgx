SymbolRate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut:SRATe:STATe

.. code-block:: python

	OUTPut:SRATe:STATe



.. autoclass:: RsNgx.Implementations.Output.SymbolRate.SymbolRateCls
	:members:
	:undoc-members:
	:noindex: