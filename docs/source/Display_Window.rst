Window
----------------------------------------





.. autoclass:: RsNgx.Implementations.Display.Window.WindowCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Display_Window_Text.rst