Umin
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALibration:AINPut:UMIN

.. code-block:: python

	CALibration:AINPut:UMIN



.. autoclass:: RsNgx.Implementations.Calibration.Ainput.Umin.UminCls
	:members:
	:undoc-members:
	:noindex: