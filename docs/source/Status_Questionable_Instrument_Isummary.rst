Isummary<Channel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.status.questionable.instrument.isummary.repcap_channel_get()
	driver.status.questionable.instrument.isummary.repcap_channel_set(repcap.Channel.Nr1)





.. autoclass:: RsNgx.Implementations.Status.Questionable.Instrument.Isummary.IsummaryCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Status_Questionable_Instrument_Isummary_Condition.rst
	Status_Questionable_Instrument_Isummary_Enable.rst
	Status_Questionable_Instrument_Isummary_Event.rst
	Status_Questionable_Instrument_Isummary_Ntransition.rst
	Status_Questionable_Instrument_Isummary_Ptransition.rst