Event
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:OPERation:INSTrument:ISUMmary<Channel>:EVENt

.. code-block:: python

	STATus:OPERation:INSTrument:ISUMmary<Channel>:EVENt



.. autoclass:: RsNgx.Implementations.Status.Operation.Instrument.Isummary.Event.EventCls
	:members:
	:undoc-members:
	:noindex: