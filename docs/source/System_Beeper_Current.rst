Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:BEEPer:CURRent:STATe

.. code-block:: python

	SYSTem:BEEPer:CURRent:STATe



.. autoclass:: RsNgx.Implementations.System.Beeper.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: