Vnc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:VNC:STATe
	single: SYSTem:VNC:PORT

.. code-block:: python

	SYSTem:VNC:STATe
	SYSTem:VNC:PORT



.. autoclass:: RsNgx.Implementations.System.Vnc.VncCls
	:members:
	:undoc-members:
	:noindex: