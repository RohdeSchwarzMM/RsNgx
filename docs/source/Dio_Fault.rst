Fault
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIO:FAULt:STATe
	single: DIO:FAULt:CHANnel
	single: DIO:FAULt:SOURce
	single: DIO:FAULt:SIGNal

.. code-block:: python

	DIO:FAULt:STATe
	DIO:FAULt:CHANnel
	DIO:FAULt:SOURce
	DIO:FAULt:SIGNal



.. autoclass:: RsNgx.Implementations.Dio.Fault.FaultCls
	:members:
	:undoc-members:
	:noindex: