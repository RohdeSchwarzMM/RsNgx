Apply
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:SOCKet:APPLy

.. code-block:: python

	SYSTem:COMMunicate:SOCKet:APPLy



.. autoclass:: RsNgx.Implementations.System.Communicate.Socket.Apply.ApplyCls
	:members:
	:undoc-members:
	:noindex: