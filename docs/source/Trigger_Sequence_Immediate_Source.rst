Source
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:IMMediate:SOURce

.. code-block:: python

	TRIGger:SEQuence:IMMediate:SOURce



.. autoclass:: RsNgx.Implementations.Trigger.Sequence.Immediate.Source.SourceCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Sequence_Immediate_Source_Dio.rst
	Trigger_Sequence_Immediate_Source_Omode.rst
	Trigger_Sequence_Immediate_Source_Output.rst