Current
----------------------------------------





.. autoclass:: RsNgx.Implementations.Measure.Scalar.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Measure_Scalar_Current_Dc.rst