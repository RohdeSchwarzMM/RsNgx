Current
----------------------------------------





.. autoclass:: RsNgx.Implementations.Sense.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Current_Range.rst