Point
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ARBitrary:TRIGgered:POINt:STATe

.. code-block:: python

	ARBitrary:TRIGgered:POINt:STATe



.. autoclass:: RsNgx.Implementations.Arbitrary.Triggered.Point.PointCls
	:members:
	:undoc-members:
	:noindex: