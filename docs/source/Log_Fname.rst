Fname
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LOG:FNAMe

.. code-block:: python

	LOG:FNAMe



.. autoclass:: RsNgx.Implementations.Log.Fname.FnameCls
	:members:
	:undoc-members:
	:noindex: