Voltage
----------------------------------------





.. autoclass:: RsNgx.Implementations.Measure.Scalar.Voltage.VoltageCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Measure_Scalar_Voltage_Dc.rst