Ainput
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALibration:AINPut:SAVE
	single: CALibration:AINPut:DATA
	single: CALibration:AINPut:DATE
	single: CALibration:AINPut:COUNt

.. code-block:: python

	CALibration:AINPut:SAVE
	CALibration:AINPut:DATA
	CALibration:AINPut:DATE
	CALibration:AINPut:COUNt



.. autoclass:: RsNgx.Implementations.Calibration.Ainput.AinputCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calibration_Ainput_Cancel.rst
	Calibration_Ainput_End.rst
	Calibration_Ainput_Start.rst
	Calibration_Ainput_Umax.rst
	Calibration_Ainput_Umin.rst