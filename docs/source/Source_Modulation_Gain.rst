Gain
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:MODulation:GAIN

.. code-block:: python

	SOURce:MODulation:GAIN



.. autoclass:: RsNgx.Implementations.Source.Modulation.Gain.GainCls
	:members:
	:undoc-members:
	:noindex: