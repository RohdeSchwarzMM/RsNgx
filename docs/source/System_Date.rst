Date
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:DATE

.. code-block:: python

	SYSTem:DATE



.. autoclass:: RsNgx.Implementations.System.Date.DateCls
	:members:
	:undoc-members:
	:noindex: