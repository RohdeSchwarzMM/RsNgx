Arbitrary
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ARBitrary:TRANsfer
	single: ARBitrary:STATe
	single: ARBitrary:DATA
	single: ARBitrary:REPetitions
	single: ARBitrary:CLEar
	single: ARBitrary:SAVE
	single: ARBitrary:LOAD

.. code-block:: python

	ARBitrary:TRANsfer
	ARBitrary:STATe
	ARBitrary:DATA
	ARBitrary:REPetitions
	ARBitrary:CLEar
	ARBitrary:SAVE
	ARBitrary:LOAD



.. autoclass:: RsNgx.Implementations.Arbitrary.ArbitraryCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Arbitrary_Behavior.rst
	Arbitrary_Block.rst
	Arbitrary_Fname.rst
	Arbitrary_Priority.rst
	Arbitrary_Sequence.rst
	Arbitrary_Triggered.rst