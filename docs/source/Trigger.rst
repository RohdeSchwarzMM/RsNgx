Trigger
----------------------------------------





.. autoclass:: RsNgx.Implementations.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Channel.rst
	Trigger_Condition.rst
	Trigger_Direction.rst
	Trigger_Enable.rst
	Trigger_Logic.rst
	Trigger_Sequence.rst
	Trigger_State.rst