Status
----------------------------------------





.. autoclass:: RsNgx.Implementations.Status.StatusCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Status_Operation.rst
	Status_Questionable.rst