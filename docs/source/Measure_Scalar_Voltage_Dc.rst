Dc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MEASure:SCALar:VOLTage:DC:MAX
	single: MEASure:SCALar:VOLTage:DC:AVG
	single: MEASure:SCALar:VOLTage:DC:MIN
	single: MEASure:SCALar:VOLTage:DC:STATistic
	single: MEASure:SCALar:VOLTage:DC

.. code-block:: python

	MEASure:SCALar:VOLTage:DC:MAX
	MEASure:SCALar:VOLTage:DC:AVG
	MEASure:SCALar:VOLTage:DC:MIN
	MEASure:SCALar:VOLTage:DC:STATistic
	MEASure:SCALar:VOLTage:DC



.. autoclass:: RsNgx.Implementations.Measure.Scalar.Voltage.Dc.DcCls
	:members:
	:undoc-members:
	:noindex: