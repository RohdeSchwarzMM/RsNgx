Apply
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:WLAN:APPLy

.. code-block:: python

	SYSTem:COMMunicate:WLAN:APPLy



.. autoclass:: RsNgx.Implementations.System.Communicate.Wlan.Apply.ApplyCls
	:members:
	:undoc-members:
	:noindex: