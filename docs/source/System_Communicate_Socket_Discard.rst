Discard
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:SOCKet:DISCard

.. code-block:: python

	SYSTem:COMMunicate:SOCKet:DISCard



.. autoclass:: RsNgx.Implementations.System.Communicate.Socket.Discard.DiscardCls
	:members:
	:undoc-members:
	:noindex: