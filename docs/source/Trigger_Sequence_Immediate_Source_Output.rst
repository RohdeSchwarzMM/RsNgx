Output
----------------------------------------





.. autoclass:: RsNgx.Implementations.Trigger.Sequence.Immediate.Source.Output.OutputCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Sequence_Immediate_Source_Output_Channel.rst