Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:OPERation:INSTrument:ISUMmary<Channel>:PTRansition

.. code-block:: python

	STATus:OPERation:INSTrument:ISUMmary<Channel>:PTRansition



.. autoclass:: RsNgx.Implementations.Status.Operation.Instrument.Isummary.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex: