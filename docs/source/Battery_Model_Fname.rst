Fname
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: BATTery:MODel:FNAMe

.. code-block:: python

	BATTery:MODel:FNAMe



.. autoclass:: RsNgx.Implementations.Battery.Model.Fname.FnameCls
	:members:
	:undoc-members:
	:noindex: