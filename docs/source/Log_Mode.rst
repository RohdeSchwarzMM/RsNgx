Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LOG:MODE

.. code-block:: python

	LOG:MODE



.. autoclass:: RsNgx.Implementations.Log.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: