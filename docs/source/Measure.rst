Measure
----------------------------------------





.. autoclass:: RsNgx.Implementations.Measure.MeasureCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Measure_Scalar.rst
	Measure_Voltage.rst