Imax
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALibration:CURRent:IMAX

.. code-block:: python

	CALibration:CURRent:IMAX



.. autoclass:: RsNgx.Implementations.Calibration.Current.Imax.ImaxCls
	:members:
	:undoc-members:
	:noindex: