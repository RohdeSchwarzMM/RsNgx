File
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FLOG:FILE:TPARtition
	single: FLOG:FILE:DURation

.. code-block:: python

	FLOG:FILE:TPARtition
	FLOG:FILE:DURation



.. autoclass:: RsNgx.Implementations.Flog.File.FileCls
	:members:
	:undoc-members:
	:noindex: