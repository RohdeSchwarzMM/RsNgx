Link
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FUSE:LINK

.. code-block:: python

	FUSE:LINK



.. autoclass:: RsNgx.Implementations.Fuse.Link.LinkCls
	:members:
	:undoc-members:
	:noindex: