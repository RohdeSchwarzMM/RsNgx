Dio<DigitalIo>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.trigger.logic.dio.repcap_digitalIo_get()
	driver.trigger.logic.dio.repcap_digitalIo_set(repcap.DigitalIo.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:LOGic:DIO<DigitalIo>

.. code-block:: python

	TRIGger:LOGic:DIO<DigitalIo>



.. autoclass:: RsNgx.Implementations.Trigger.Logic.Dio.DioCls
	:members:
	:undoc-members:
	:noindex: