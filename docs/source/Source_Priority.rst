Priority
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:PRIority

.. code-block:: python

	SOURce:PRIority



.. autoclass:: RsNgx.Implementations.Source.Priority.PriorityCls
	:members:
	:undoc-members:
	:noindex: