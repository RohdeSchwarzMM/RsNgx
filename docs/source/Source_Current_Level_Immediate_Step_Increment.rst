Increment
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:CURRent:LEVel:IMMediate:STEP:INCRement

.. code-block:: python

	SOURce:CURRent:LEVel:IMMediate:STEP:INCRement



.. autoclass:: RsNgx.Implementations.Source.Current.Level.Immediate.Step.Increment.IncrementCls
	:members:
	:undoc-members:
	:noindex: