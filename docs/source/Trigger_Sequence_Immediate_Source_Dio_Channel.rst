Channel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:IMMediate:SOURce:DIO:CHANnel

.. code-block:: python

	TRIGger:SEQuence:IMMediate:SOURce:DIO:CHANnel



.. autoclass:: RsNgx.Implementations.Trigger.Sequence.Immediate.Source.Dio.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex: