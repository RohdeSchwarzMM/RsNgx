Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:OPERation:INSTrument:ISUMmary<Channel>:NTRansition

.. code-block:: python

	STATus:OPERation:INSTrument:ISUMmary<Channel>:NTRansition



.. autoclass:: RsNgx.Implementations.Status.Operation.Instrument.Isummary.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex: