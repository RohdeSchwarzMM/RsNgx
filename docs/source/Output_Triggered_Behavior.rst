Behavior
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut:TRIGgered:BEHavior

.. code-block:: python

	OUTPut:TRIGgered:BEHavior



.. autoclass:: RsNgx.Implementations.Output.Triggered.Behavior.BehaviorCls
	:members:
	:undoc-members:
	:noindex: