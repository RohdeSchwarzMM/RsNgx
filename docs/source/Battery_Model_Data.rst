Data
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: BATTery:MODel:DATA

.. code-block:: python

	BATTery:MODel:DATA



.. autoclass:: RsNgx.Implementations.Battery.Model.Data.DataCls
	:members:
	:undoc-members:
	:noindex: