Ntransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:INSTrument:ISUMmary<Channel>:NTRansition

.. code-block:: python

	STATus:QUEStionable:INSTrument:ISUMmary<Channel>:NTRansition



.. autoclass:: RsNgx.Implementations.Status.Questionable.Instrument.Isummary.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex: