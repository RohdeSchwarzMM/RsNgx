Immediate
----------------------------------------





.. autoclass:: RsNgx.Implementations.Trigger.Sequence.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Sequence_Immediate_Source.rst