Sense
----------------------------------------





.. autoclass:: RsNgx.Implementations.Sense.SenseCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Current.rst
	Sense_NplCycles.rst
	Sense_Voltage.rst