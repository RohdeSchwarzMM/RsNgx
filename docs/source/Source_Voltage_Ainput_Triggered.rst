Triggered
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:VOLTage:AINPut:TRIGgered:STATe

.. code-block:: python

	SOURce:VOLTage:AINPut:TRIGgered:STATe



.. autoclass:: RsNgx.Implementations.Source.Voltage.Ainput.Triggered.TriggeredCls
	:members:
	:undoc-members:
	:noindex: