Stime
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LOG:STIMe

.. code-block:: python

	LOG:STIMe



.. autoclass:: RsNgx.Implementations.Log.Stime.StimeCls
	:members:
	:undoc-members:
	:noindex: