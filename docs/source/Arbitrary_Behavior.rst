Behavior
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ARBitrary:BEHavior:END

.. code-block:: python

	ARBitrary:BEHavior:END



.. autoclass:: RsNgx.Implementations.Arbitrary.Behavior.BehaviorCls
	:members:
	:undoc-members:
	:noindex: