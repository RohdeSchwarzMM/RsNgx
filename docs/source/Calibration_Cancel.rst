Cancel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALibration:CANCel

.. code-block:: python

	CALibration:CANCel



.. autoclass:: RsNgx.Implementations.Calibration.Cancel.CancelCls
	:members:
	:undoc-members:
	:noindex: