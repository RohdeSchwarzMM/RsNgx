Dio<DigitalIo>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.trigger.channel.dio.repcap_digitalIo_get()
	driver.trigger.channel.dio.repcap_digitalIo_set(repcap.DigitalIo.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:CHANnel:DIO<DigitalIo>

.. code-block:: python

	TRIGger:CHANnel:DIO<DigitalIo>



.. autoclass:: RsNgx.Implementations.Trigger.Channel.Dio.DioCls
	:members:
	:undoc-members:
	:noindex: