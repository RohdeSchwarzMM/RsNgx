Start
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALibration:AINPut:STARt

.. code-block:: python

	CALibration:AINPut:STARt



.. autoclass:: RsNgx.Implementations.Calibration.Ainput.Start.StartCls
	:members:
	:undoc-members:
	:noindex: