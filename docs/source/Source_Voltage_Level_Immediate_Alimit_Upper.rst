Upper
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:VOLTage:LEVel:IMMediate:ALIMit:UPPer

.. code-block:: python

	SOURce:VOLTage:LEVel:IMMediate:ALIMit:UPPer



.. autoclass:: RsNgx.Implementations.Source.Voltage.Level.Immediate.Alimit.Upper.UpperCls
	:members:
	:undoc-members:
	:noindex: