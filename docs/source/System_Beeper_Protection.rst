Protection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:BEEPer:PROTection:STATe

.. code-block:: python

	SYSTem:BEEPer:PROTection:STATe



.. autoclass:: RsNgx.Implementations.System.Beeper.Protection.ProtectionCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Beeper_Protection_Immediate.rst