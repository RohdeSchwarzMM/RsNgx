Range
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:VOLTage:RANGe:UPPer
	single: SENSe:VOLTage:RANGe:LOWer
	single: SENSe:VOLTage:RANGe:AUTO

.. code-block:: python

	SENSe:VOLTage:RANGe:UPPer
	SENSe:VOLTage:RANGe:LOWer
	SENSe:VOLTage:RANGe:AUTO



.. autoclass:: RsNgx.Implementations.Sense.Voltage.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: