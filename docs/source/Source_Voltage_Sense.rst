Sense
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:VOLTage:SENSe:SOURce

.. code-block:: python

	SOURce:VOLTage:SENSe:SOURce



.. autoclass:: RsNgx.Implementations.Source.Voltage.Sense.SenseCls
	:members:
	:undoc-members:
	:noindex: