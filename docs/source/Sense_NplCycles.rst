NplCycles
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:NPLCycles

.. code-block:: python

	SENSe:NPLCycles



.. autoclass:: RsNgx.Implementations.Sense.NplCycles.NplCyclesCls
	:members:
	:undoc-members:
	:noindex: