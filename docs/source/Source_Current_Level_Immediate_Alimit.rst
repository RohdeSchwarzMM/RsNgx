Alimit
----------------------------------------





.. autoclass:: RsNgx.Implementations.Source.Current.Level.Immediate.Alimit.AlimitCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Current_Level_Immediate_Alimit_Lower.rst
	Source_Current_Level_Immediate_Alimit_Upper.rst