Imin
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALibration:CURRent:IMIN

.. code-block:: python

	CALibration:CURRent:IMIN



.. autoclass:: RsNgx.Implementations.Calibration.Current.Imin.IminCls
	:members:
	:undoc-members:
	:noindex: