Calibration
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALibration:DATE
	single: CALibration:TYPE
	single: CALibration:VALue
	single: CALibration:USER
	single: CALibration:SAVE
	single: CALibration:TEMPerature
	single: CALibration:COUNt

.. code-block:: python

	CALibration:DATE
	CALibration:TYPE
	CALibration:VALue
	CALibration:USER
	CALibration:SAVE
	CALibration:TEMPerature
	CALibration:COUNt



.. autoclass:: RsNgx.Implementations.Calibration.CalibrationCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calibration_Ainput.rst
	Calibration_Cancel.rst
	Calibration_Current.rst
	Calibration_End.rst
	Calibration_Point.rst
	Calibration_Self.rst
	Calibration_Voltage.rst