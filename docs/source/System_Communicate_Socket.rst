Socket
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:SOCKet:DHCP
	single: SYSTem:COMMunicate:SOCKet:IPADdress
	single: SYSTem:COMMunicate:SOCKet:MASK
	single: SYSTem:COMMunicate:SOCKet:GATeway
	single: SYSTem:COMMunicate:SOCKet:RESet

.. code-block:: python

	SYSTem:COMMunicate:SOCKet:DHCP
	SYSTem:COMMunicate:SOCKet:IPADdress
	SYSTem:COMMunicate:SOCKet:MASK
	SYSTem:COMMunicate:SOCKet:GATeway
	SYSTem:COMMunicate:SOCKet:RESet



.. autoclass:: RsNgx.Implementations.System.Communicate.Socket.SocketCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Communicate_Socket_Apply.rst
	System_Communicate_Socket_Discard.rst