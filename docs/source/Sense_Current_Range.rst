Range
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:CURRent:RANGe:UPPer
	single: SENSe:CURRent:RANGe:LOWer
	single: SENSe:CURRent:RANGe:AUTO

.. code-block:: python

	SENSe:CURRent:RANGe:UPPer
	SENSe:CURRent:RANGe:LOWer
	SENSe:CURRent:RANGe:AUTO



.. autoclass:: RsNgx.Implementations.Sense.Current.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: