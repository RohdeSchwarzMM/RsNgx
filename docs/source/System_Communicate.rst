Communicate
----------------------------------------





.. autoclass:: RsNgx.Implementations.System.Communicate.CommunicateCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Communicate_Lan.rst
	System_Communicate_Socket.rst
	System_Communicate_Wlan.rst