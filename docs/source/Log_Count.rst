Count
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: LOG:COUNt

.. code-block:: python

	LOG:COUNt



.. autoclass:: RsNgx.Implementations.Log.Count.CountCls
	:members:
	:undoc-members:
	:noindex: