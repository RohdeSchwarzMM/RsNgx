Connection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:WLAN:CONNection:STATe

.. code-block:: python

	SYSTem:COMMunicate:WLAN:CONNection:STATe



.. autoclass:: RsNgx.Implementations.System.Communicate.Wlan.Connection.ConnectionCls
	:members:
	:undoc-members:
	:noindex: