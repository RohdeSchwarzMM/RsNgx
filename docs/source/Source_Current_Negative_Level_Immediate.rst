Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:CURRent:NEGative:LEVel:IMMediate:AMPLitude

.. code-block:: python

	SOURce:CURRent:NEGative:LEVel:IMMediate:AMPLitude



.. autoclass:: RsNgx.Implementations.Source.Current.Negative.Level.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex: