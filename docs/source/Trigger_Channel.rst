Channel
----------------------------------------





.. autoclass:: RsNgx.Implementations.Trigger.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Channel_Dio.rst