Ptransition
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:INSTrument:ISUMmary<Channel>:PTRansition

.. code-block:: python

	STATus:QUEStionable:INSTrument:ISUMmary<Channel>:PTRansition



.. autoclass:: RsNgx.Implementations.Status.Questionable.Instrument.Isummary.Ptransition.PtransitionCls
	:members:
	:undoc-members:
	:noindex: