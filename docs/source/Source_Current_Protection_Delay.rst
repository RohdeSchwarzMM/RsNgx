Delay
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:CURRent:PROTection:DELay:INITial
	single: SOURce:CURRent:PROTection:DELay

.. code-block:: python

	SOURce:CURRent:PROTection:DELay:INITial
	SOURce:CURRent:PROTection:DELay



.. autoclass:: RsNgx.Implementations.Source.Current.Protection.Delay.DelayCls
	:members:
	:undoc-members:
	:noindex: