Voc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: BATTery:SIMulator:VOC:FULL
	single: BATTery:SIMulator:VOC:EMPTy
	single: BATTery:SIMulator:VOC

.. code-block:: python

	BATTery:SIMulator:VOC:FULL
	BATTery:SIMulator:VOC:EMPTy
	BATTery:SIMulator:VOC



.. autoclass:: RsNgx.Implementations.Battery.Simulator.Voc.VocCls
	:members:
	:undoc-members:
	:noindex: