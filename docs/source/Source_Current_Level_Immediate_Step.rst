Step
----------------------------------------





.. autoclass:: RsNgx.Implementations.Source.Current.Level.Immediate.Step.StepCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Current_Level_Immediate_Step_Increment.rst