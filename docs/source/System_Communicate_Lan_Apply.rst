Apply
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:COMMunicate:LAN:APPLy

.. code-block:: python

	SYSTem:COMMunicate:LAN:APPLy



.. autoclass:: RsNgx.Implementations.System.Communicate.Lan.Apply.ApplyCls
	:members:
	:undoc-members:
	:noindex: