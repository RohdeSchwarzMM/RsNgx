Simulator
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: BATTery:SIMulator:ENABle
	single: BATTery:SIMulator:SOC
	single: BATTery:SIMulator:RESistance
	single: BATTery:SIMulator:TVOLtage

.. code-block:: python

	BATTery:SIMulator:ENABle
	BATTery:SIMulator:SOC
	BATTery:SIMulator:RESistance
	BATTery:SIMulator:TVOLtage



.. autoclass:: RsNgx.Implementations.Battery.Simulator.SimulatorCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Battery_Simulator_Capacity.rst
	Battery_Simulator_Current.rst
	Battery_Simulator_Voc.rst