Instrument
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:QUEStionable:INSTrument:EVENt
	single: STATus:QUEStionable:INSTrument:CONDition
	single: STATus:QUEStionable:INSTrument:PTRansition
	single: STATus:QUEStionable:INSTrument:NTRansition
	single: STATus:QUEStionable:INSTrument:ENABle

.. code-block:: python

	STATus:QUEStionable:INSTrument:EVENt
	STATus:QUEStionable:INSTrument:CONDition
	STATus:QUEStionable:INSTrument:PTRansition
	STATus:QUEStionable:INSTrument:NTRansition
	STATus:QUEStionable:INSTrument:ENABle



.. autoclass:: RsNgx.Implementations.Status.Questionable.Instrument.InstrumentCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Status_Questionable_Instrument_Isummary.rst