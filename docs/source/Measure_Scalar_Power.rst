Power
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MEASure:SCALar:POWer:MAX
	single: MEASure:SCALar:POWer:AVG
	single: MEASure:SCALar:POWer:MIN
	single: MEASure:SCALar:POWer:STATistic
	single: MEASure:SCALar:POWer

.. code-block:: python

	MEASure:SCALar:POWer:MAX
	MEASure:SCALar:POWer:AVG
	MEASure:SCALar:POWer:MIN
	MEASure:SCALar:POWer:STATistic
	MEASure:SCALar:POWer



.. autoclass:: RsNgx.Implementations.Measure.Scalar.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: