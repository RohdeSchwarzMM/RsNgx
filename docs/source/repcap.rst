RepCaps
=========

Channel
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Channel.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

DigitalIo
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.DigitalIo.Nr1
	# Range:
	Nr1 .. Nr8
	# All values (8x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8

