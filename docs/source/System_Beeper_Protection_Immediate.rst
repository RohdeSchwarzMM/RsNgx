Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:BEEPer:PROTection:IMMediate

.. code-block:: python

	SYSTem:BEEPer:PROTection:IMMediate



.. autoclass:: RsNgx.Implementations.System.Beeper.Protection.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex: