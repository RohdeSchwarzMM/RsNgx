Dc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MEASure:SCALar:CURRent:DC:MAX
	single: MEASure:SCALar:CURRent:DC:AVG
	single: MEASure:SCALar:CURRent:DC:MIN
	single: MEASure:SCALar:CURRent:DC:STATistic
	single: MEASure:SCALar:CURRent:DC

.. code-block:: python

	MEASure:SCALar:CURRent:DC:MAX
	MEASure:SCALar:CURRent:DC:AVG
	MEASure:SCALar:CURRent:DC:MIN
	MEASure:SCALar:CURRent:DC:STATistic
	MEASure:SCALar:CURRent:DC



.. autoclass:: RsNgx.Implementations.Measure.Scalar.Current.Dc.DcCls
	:members:
	:undoc-members:
	:noindex: