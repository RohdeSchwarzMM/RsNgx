Impedance
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: OUTPut:IMPedance:STATe
	single: OUTPut:IMPedance

.. code-block:: python

	OUTPut:IMPedance:STATe
	OUTPut:IMPedance



.. autoclass:: RsNgx.Implementations.Output.Impedance.ImpedanceCls
	:members:
	:undoc-members:
	:noindex: