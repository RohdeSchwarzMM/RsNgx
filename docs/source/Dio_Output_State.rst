State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIO:OUTPut:STATe

.. code-block:: python

	DIO:OUTPut:STATe



.. autoclass:: RsNgx.Implementations.Dio.Output.State.StateCls
	:members:
	:undoc-members:
	:noindex: