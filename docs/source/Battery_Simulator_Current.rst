Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: BATTery:SIMulator:CURRent

.. code-block:: python

	BATTery:SIMulator:CURRent



.. autoclass:: RsNgx.Implementations.Battery.Simulator.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Battery_Simulator_Current_Limit.rst