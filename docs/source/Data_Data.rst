Data
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DATA:DATA

.. code-block:: python

	DATA:DATA



.. autoclass:: RsNgx.Implementations.Data.Data.DataCls
	:members:
	:undoc-members:
	:noindex: