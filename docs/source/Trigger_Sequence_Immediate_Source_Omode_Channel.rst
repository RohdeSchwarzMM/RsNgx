Channel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:IMMediate:SOURce:OMODe:CHANnel

.. code-block:: python

	TRIGger:SEQuence:IMMediate:SOURce:OMODe:CHANnel



.. autoclass:: RsNgx.Implementations.Trigger.Sequence.Immediate.Source.Omode.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex: