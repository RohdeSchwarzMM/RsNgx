Ramp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:VOLTage:RAMP:STATe
	single: SOURce:VOLTage:RAMP:DURation

.. code-block:: python

	SOURce:VOLTage:RAMP:STATe
	SOURce:VOLTage:RAMP:DURation



.. autoclass:: RsNgx.Implementations.Source.Voltage.Ramp.RampCls
	:members:
	:undoc-members:
	:noindex: