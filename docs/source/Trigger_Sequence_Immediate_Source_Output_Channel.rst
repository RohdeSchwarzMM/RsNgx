Channel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:IMMediate:SOURce:OUTPut:CHANnel

.. code-block:: python

	TRIGger:SEQuence:IMMediate:SOURce:OUTPut:CHANnel



.. autoclass:: RsNgx.Implementations.Trigger.Sequence.Immediate.Source.Output.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex: