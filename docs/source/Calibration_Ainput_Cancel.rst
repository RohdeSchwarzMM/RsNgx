Cancel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALibration:AINPut:CANCel

.. code-block:: python

	CALibration:AINPut:CANCel



.. autoclass:: RsNgx.Implementations.Calibration.Ainput.Cancel.CancelCls
	:members:
	:undoc-members:
	:noindex: