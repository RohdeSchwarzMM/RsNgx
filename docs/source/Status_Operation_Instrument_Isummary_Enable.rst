Enable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: STATus:OPERation:INSTrument:ISUMmary<Channel>:ENABle

.. code-block:: python

	STATus:OPERation:INSTrument:ISUMmary<Channel>:ENABle



.. autoclass:: RsNgx.Implementations.Status.Operation.Instrument.Isummary.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: