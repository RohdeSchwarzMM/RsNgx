Battery
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: BATTery:STATus

.. code-block:: python

	BATTery:STATus



.. autoclass:: RsNgx.Implementations.Battery.BatteryCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Battery_Model.rst
	Battery_Simulator.rst