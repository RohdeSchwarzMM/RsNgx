Display
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DISPlay:BRIGhtness

.. code-block:: python

	DISPlay:BRIGhtness



.. autoclass:: RsNgx.Implementations.Display.DisplayCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Display_Window.rst