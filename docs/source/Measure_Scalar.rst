Scalar
----------------------------------------





.. autoclass:: RsNgx.Implementations.Measure.Scalar.ScalarCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Measure_Scalar_Current.rst
	Measure_Scalar_Energy.rst
	Measure_Scalar_Power.rst
	Measure_Scalar_Statistic.rst
	Measure_Scalar_Voltage.rst