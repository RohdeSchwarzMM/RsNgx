Group
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ARBitrary:TRIGgered:GROup:STATe

.. code-block:: python

	ARBitrary:TRIGgered:GROup:STATe



.. autoclass:: RsNgx.Implementations.Arbitrary.Triggered.Group.GroupCls
	:members:
	:undoc-members:
	:noindex: