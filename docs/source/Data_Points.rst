Points
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DATA:POINts

.. code-block:: python

	DATA:POINts



.. autoclass:: RsNgx.Implementations.Data.Points.PointsCls
	:members:
	:undoc-members:
	:noindex: