Immediate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:CURRent:LEVel:IMMediate:AMPLitude

.. code-block:: python

	SOURce:CURRent:LEVel:IMMediate:AMPLitude



.. autoclass:: RsNgx.Implementations.Source.Current.Level.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Current_Level_Immediate_Alimit.rst
	Source_Current_Level_Immediate_Step.rst