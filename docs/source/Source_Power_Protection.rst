Protection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:POWer:PROTection:STATe
	single: SOURce:POWer:PROTection:LEVel
	single: SOURce:POWer:PROTection:TRIPped
	single: SOURce:POWer:PROTection:CLEar

.. code-block:: python

	SOURce:POWer:PROTection:STATe
	SOURce:POWer:PROTection:LEVel
	SOURce:POWer:PROTection:TRIPped
	SOURce:POWer:PROTection:CLEar



.. autoclass:: RsNgx.Implementations.Source.Power.Protection.ProtectionCls
	:members:
	:undoc-members:
	:noindex: