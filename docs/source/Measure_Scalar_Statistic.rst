Statistic
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: MEASure:SCALar:STATistic:RESet
	single: MEASure:SCALar:STATistic:COUNt

.. code-block:: python

	MEASure:SCALar:STATistic:RESet
	MEASure:SCALar:STATistic:COUNt



.. autoclass:: RsNgx.Implementations.Measure.Scalar.Statistic.StatisticCls
	:members:
	:undoc-members:
	:noindex: