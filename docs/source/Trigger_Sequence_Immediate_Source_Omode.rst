Omode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:IMMediate:SOURce:OMODe

.. code-block:: python

	TRIGger:SEQuence:IMMediate:SOURce:OMODe



.. autoclass:: RsNgx.Implementations.Trigger.Sequence.Immediate.Source.Omode.OmodeCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Sequence_Immediate_Source_Omode_Channel.rst