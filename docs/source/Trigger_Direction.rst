Direction
----------------------------------------





.. autoclass:: RsNgx.Implementations.Trigger.Direction.DirectionCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Direction_Dio.rst