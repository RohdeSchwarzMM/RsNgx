Key
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SYSTem:KEY:BRIGhtness

.. code-block:: python

	SYSTem:KEY:BRIGhtness



.. autoclass:: RsNgx.Implementations.System.Key.KeyCls
	:members:
	:undoc-members:
	:noindex: