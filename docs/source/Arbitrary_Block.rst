Block
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ARBitrary:BLOCk:DATA
	single: ARBitrary:BLOCk:REPetitions
	single: ARBitrary:BLOCk:ENDPoint
	single: ARBitrary:BLOCk:CLEar
	single: ARBitrary:BLOCk

.. code-block:: python

	ARBitrary:BLOCk:DATA
	ARBitrary:BLOCk:REPetitions
	ARBitrary:BLOCk:ENDPoint
	ARBitrary:BLOCk:CLEar
	ARBitrary:BLOCk



.. autoclass:: RsNgx.Implementations.Arbitrary.Block.BlockCls
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Arbitrary_Block_Fname.rst