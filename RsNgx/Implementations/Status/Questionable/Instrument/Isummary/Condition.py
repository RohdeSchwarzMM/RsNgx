from ......Internal.Core import Core
from ......Internal.CommandsGroup import CommandsGroup
from ......Internal import Conversions
from ...... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class ConditionCls:
	"""Condition commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("condition", core, parent)

	def get(self, channel=repcap.Channel.Default) -> int:
		"""SCPI: STATus:QUEStionable:INSTrument:ISUMmary<Channel>:CONDition \n
		Snippet: value: int = driver.status.questionable.instrument.isummary.condition.get(channel = repcap.Channel.Default) \n
		Returns the contents of the CONDition part of the status register to check for questionable instrument or measurement
		states. Reading the CONDition registers does not delete the contents. \n
			:param channel: optional repeated capability selector. Default value: Nr1 (settable in the interface 'Isummary')
			:return: result: Condition bits in decimal representation"""
		channel_cmd_val = self._cmd_group.get_repcap_cmd_value(channel, repcap.Channel)
		response = self._core.io.query_str(f'STATus:QUEStionable:INSTrument:ISUMmary{channel_cmd_val}:CONDition?')
		return Conversions.str_to_int(response)
