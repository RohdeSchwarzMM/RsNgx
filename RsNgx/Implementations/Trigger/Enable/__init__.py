from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class EnableCls:
	"""Enable commands group definition. 3 total commands, 2 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("enable", core, parent)

	@property
	def dio(self):
		"""dio commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_dio'):
			from .Dio import DioCls
			self._dio = DioCls(self._core, self._cmd_group)
		return self._dio

	@property
	def select(self):
		"""select commands group. 1 Sub-classes, 0 commands."""
		if not hasattr(self, '_select'):
			from .Select import SelectCls
			self._select = SelectCls(self._core, self._cmd_group)
		return self._select

	def get_general(self) -> bool:
		"""SCPI: TRIGger[:ENABle]:GENeral \n
		Snippet: value: bool = driver.trigger.enable.get_general() \n
		Sets or queries the enable state of the master on/off of Digital I/O trigger. \n
			:return: arg_0: No help available
		"""
		response = self._core.io.query_str('TRIGger:ENABle:GENeral?')
		return Conversions.str_to_bool(response)

	def set_general(self, arg_0: bool) -> None:
		"""SCPI: TRIGger[:ENABle]:GENeral \n
		Snippet: driver.trigger.enable.set_general(arg_0 = False) \n
		Sets or queries the enable state of the master on/off of Digital I/O trigger. \n
			:param arg_0:
				- 1: Master state of Digital I/O trigger is enabled.
				- 0: Master state of Digital I/O trigger is disabled."""
		param = Conversions.bool_to_str(arg_0)
		self._core.io.write(f'TRIGger:ENABle:GENeral {param}')
