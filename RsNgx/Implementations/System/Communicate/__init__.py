from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class CommunicateCls:
	"""Communicate commands group definition. 26 total commands, 3 Subgroups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("communicate", core, parent)

	@property
	def socket(self):
		"""socket commands group. 2 Sub-classes, 5 commands."""
		if not hasattr(self, '_socket'):
			from .Socket import SocketCls
			self._socket = SocketCls(self._core, self._cmd_group)
		return self._socket

	@property
	def wlan(self):
		"""wlan commands group. 2 Sub-classes, 7 commands."""
		if not hasattr(self, '_wlan'):
			from .Wlan import WlanCls
			self._wlan = WlanCls(self._core, self._cmd_group)
		return self._wlan

	@property
	def lan(self):
		"""lan commands group. 2 Sub-classes, 8 commands."""
		if not hasattr(self, '_lan'):
			from .Lan import LanCls
			self._lan = LanCls(self._core, self._cmd_group)
		return self._lan
