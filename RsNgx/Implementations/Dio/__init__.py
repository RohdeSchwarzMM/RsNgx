from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class DioCls:
	"""Dio commands group definition. 7 total commands, 2 Subgroups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("dio", core, parent)

	@property
	def fault(self):
		"""fault commands group. 0 Sub-classes, 4 commands."""
		if not hasattr(self, '_fault'):
			from .Fault import FaultCls
			self._fault = FaultCls(self._core, self._cmd_group)
		return self._fault

	@property
	def output(self):
		"""output commands group. 3 Sub-classes, 0 commands."""
		if not hasattr(self, '_output'):
			from .Output import OutputCls
			self._output = OutputCls(self._core, self._cmd_group)
		return self._output
