from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class CurrentCls:
	"""Current commands group definition. 13 total commands, 3 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("current", core, parent)

	@property
	def protection(self):
		"""protection commands group. 2 Sub-classes, 4 commands."""
		if not hasattr(self, '_protection'):
			from .Protection import ProtectionCls
			self._protection = ProtectionCls(self._core, self._cmd_group)
		return self._protection

	@property
	def negative(self):
		"""negative commands group. 1 Sub-classes, 0 commands."""
		if not hasattr(self, '_negative'):
			from .Negative import NegativeCls
			self._negative = NegativeCls(self._core, self._cmd_group)
		return self._negative

	@property
	def level(self):
		"""level commands group. 1 Sub-classes, 0 commands."""
		if not hasattr(self, '_level'):
			from .Level import LevelCls
			self._level = LevelCls(self._core, self._cmd_group)
		return self._level

	def get_range(self) -> float:
		"""SCPI: [SOURce]:CURRent:RANGe \n
		Snippet: value: float = driver.source.current.get_range() \n
		No command help available \n
			:return: arg_0: No help available
		"""
		response = self._core.io.query_str('SOURce:CURRent:RANGe?')
		return Conversions.str_to_float(response)

	def set_range(self, arg_0: float) -> None:
		"""SCPI: [SOURce]:CURRent:RANGe \n
		Snippet: driver.source.current.set_range(arg_0 = 1.0) \n
		No command help available \n
			:param arg_0: No help available
		"""
		param = Conversions.decimal_value_to_str(arg_0)
		self._core.io.write(f'SOURce:CURRent:RANGe {param}')
