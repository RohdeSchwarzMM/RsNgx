from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class TriggeredCls:
	"""Triggered commands group definition. 2 total commands, 2 Subgroups, 0 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("triggered", core, parent)

	@property
	def state(self):
		"""state commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_state'):
			from .State import StateCls
			self._state = StateCls(self._core, self._cmd_group)
		return self._state

	@property
	def behavior(self):
		"""behavior commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_behavior'):
			from .Behavior import BehaviorCls
			self._behavior = BehaviorCls(self._core, self._cmd_group)
		return self._behavior
