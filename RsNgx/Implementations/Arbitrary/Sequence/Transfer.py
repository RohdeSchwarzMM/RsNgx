from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class TransferCls:
	"""Transfer commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("transfer", core, parent)

	def set(self) -> None:
		"""SCPI: ARBitrary:SEQuence:TRANsfer \n
		Snippet: driver.arbitrary.sequence.transfer.set() \n
		Transfers the defined arbitrary table to the selected channel. \n
		"""
		self._core.io.write(f'ARBitrary:SEQuence:TRANsfer')

	def set_with_opc(self, opc_timeout_ms: int = -1) -> None:
		"""SCPI: ARBitrary:SEQuence:TRANsfer \n
		Snippet: driver.arbitrary.sequence.transfer.set_with_opc() \n
		Transfers the defined arbitrary table to the selected channel. \n
		Same as set, but waits for the operation to complete before continuing further. Use the RsNgx.utilities.opc_timeout_set() to set the timeout value. \n
			:param opc_timeout_ms: Maximum time to wait in milliseconds, valid only for this call."""
		self._core.io.write_with_opc(f'ARBitrary:SEQuence:TRANsfer', opc_timeout_ms)
